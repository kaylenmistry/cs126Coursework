# CS126 Assignment - Results


# Student Id: (u1736245)

<style
type="text/css">
div.marker_feedback{color:red;font-weight:bold}
</style>

## Problem 1: Create an array list you can iterate over using a "for-each loop"


I have provided a solution to the problem by modifying [MyArrayList.java](./src/main/java/adt/list/MyArrayList.java): **Yes**

<div class='marker_feedback'>Marker feedback:</div>

## Problem 2: Investigate the class `Patient` and implement a sortable `PatientArrayList`
**a)** I have completed [PatientArrayList.java](./src/main/java/patient/PatientArrayList.java): **Yes** 

<div class='marker_feedback'>Marker feedback:</div>

**b)** I have implemented the following sorting method in [PatientArrayList.java](./src/main/java/patient/PatientArrayList.java) (describe your method):

Merge Sort

<div class='marker_feedback'>Marker feedback:</div>

**c)** The computational complexity of my sorting method in [PatientArrayList.java](./src/main/java/patient/PatientArrayList.java) is (state the complexity and discuss why you have chosen this method):

Average case and worst case is O(n log(n)). This is why I chose it over Quick Sort, since Quick Sort has a worst case complexity of O(n^2).

Also, from my research, Java's Array.sort() method uses Merge Sort for sorting Arrays of Reference Type Objects, since it is stable, that is, [equal elements keep their original order](https://stackoverflow.com/questions/29218440/when-merge-sort-is-preferred-over-quick-sort).

In addition to this, Merge Sort works well implemented in parallel, and uses, on average, less comparisons than Quick Sort, which can be computationally expensive for Objects.

<div class='marker_feedback'>Marker feedback:</div>

**d)** A list of 20 patients sorted by their Id, which is output by `problem2()` in [WirusProblemSolutions.java](./src/main/java/wirus/WirusProblemSolutions.java) (copy for example the last 20 entries of the entire list from the console, and paste them here):

6155680558 	Howard 	Jaxon 	03/02/2018 	CACGGAAAAGATGCTGGT

6155823634 	Pacheco 	Daniel 	04/02/2018 	CACGGAAAAGATGCTGGT 	

6156331855 	Walls 	Liam 	30/01/2018 	CACGGAAAAGATGCTGGT 	

6156362347 	Payne 	Justin 	26/01/2018 	CACGGAAAAGATGCTGGT 	

6156554713 	Carney 	Logan 	21/01/2018 	CACGGAAAAGATGCTGGT 	

6156560126 	Schultz 	Gianna 	04/02/2018 	CACGGAAAAGATGCTGGT 	

6156877895 	Strong 	William 	02/02/2018 	CACGGAAAAGATGCTGGT 	

6157966841 	Glover 	Aubrey 	05/02/2018 	CACGGAAAAGATGCTGGT 	

6158078472 	Adkins 	Nicholas 	04/02/2018 	CACGGAAAAGATGCTGGT 	

6158508674 	Sharpe 	Madeline 	29/01/2018 	CACGGGAAAGATGCTGGT 	

6158915088 	Rosa 	Zoey 	29/01/2018 	CACGGAAAAGATGCTGGT 	

6160242089 	Kerr 	Levi 	25/01/2018 	CACGGAAAAGATGCTGGT 	

6160394009 	Bowers 	Natalie 	04/02/2018 	CACGGAAAAGATGCTGGT 	

6161626569 	Cameron 	Genesis 	04/02/2018 	CACGGAAAAGATGCTGGT 	

6162375214 	Ramsey 	Eva 	04/02/2018 	CACGGAAAAGATGCTGGT 	

6162629271 	Mckee 	Lucy 	30/01/2018 	CACGGAAAAGATGCTGGT 	

6162706674 	Banks 	Jason 	04/02/2018 	CACGGAAAAGATGCTGGT 	

6163468094 	Kim 	Nathaniel 	26/01/2018 	CACGGAAAAGATGCTGGT 	

6163796085 	Ferguson 	Sarah 	29/01/2018 	CACGGAAAAGATGCTGGT 	

6164158354 	Dillon 	Autumn 	01/02/2018 	CACGGAAAAGATGCTGGT 	

6164331092 	Furter 	Frank_N 	02/02/2018 	CACGGGAAAGATGTTCGT 

<div class='marker_feedback'>Marker feedback:</div>

## Problem 3: Use a Set to list the different virus sequences

**a)** I have created the object `Set<String> dnaset` in `problem3a()` of [WirusProblemSolutions.java](./src/main/java/wirus/Wirus.java) : **Yes**

<div class='marker_feedback'>Marker feedback:</div>

**b)** The computational complexity of adding a new virus sequence to the set is (explain why):

Since the set is internally encoded as an array, the complexity of searching an array and inserting is O(n). However, if implemented as some form of tree, this can be reduced to O(log n).

<div class='marker_feedback'>Marker feedback:</div>


**c)** A list of patients with the virus sequence “CACGGGAAAGATGTTCGT”:

6164331092 	Furter 	Frank_N 	02/02/2018 	CACGGGAAAGATGTTCGT 	

<div class='marker_feedback'>Marker feedback:</div>

**d)** Why, in our example, would using a search tree or a HashMap not necessarily help to find patients with a specific virus sequence faster?
Explain under which conditions you could expect a dramatic speed-up, and when not.

The Set.java set implemented performs a linear search, of complexity O(n), since we must check every patient for a new virus sequence.

This can usually be improved on by using a search tree or HashMap: a balanced Binary Tree, for example, has a search time of O(log n), and HashMap can have a search time as little as O(1) under the right conditions. Such conditions will be discussed now. 

Binary Trees can have a search time of O(log n) if they are balanced, or close to balanced. However, if they are not, the structure becomes what can only be described as an expensive linked list, giving a search time of O(n)- no better than a standard linear search

For this data set, a HashMap would have only 3 buckets for 20,000 data entries, thus making it rather inefficient. Worst case scenario would yield O(n) complexity once again no better than a linear search. 

Since there are only 3 unique virus sequences, the Binary Tree generated could potentially be very imbalanced, and a HashMap could result in large buckets to walk through, hence they will not necessarily improve the search time dramatically. To see dramatic performance increases, we would need a larger number of unique entries, allowing us to create a balanced tree, and a larger number of buckets for the sample size (HashMap).

<div class='marker_feedback'>Marker feedback:</div>


## Problem 4: Use a HashMap to find patients by their Id

**a)** Using a `HashMap<Long, Patient>` in `problem4()` of [WirusProblemSolutions.java](./src/main/java/wirus/Wirus.java) I found that the patient with Id 6164331092 is:

Frank_N Furter

<div class='marker_feedback'>Marker feedback:</div>

**b)** The computational complexity of looking up an arbitrary patient by their Id using a HashMap is (explain why):

Usually O(1) (constant time), but worst case is O(n) if all distinct keys hash to the same bucket.

<div class='marker_feedback'>Marker feedback:</div>

## Problem 5: I have chosen option 5.1/5.2 (delete as appropriate)

### Problem 5.1: Number of patients admitted to hospital per calendar week

**a)** I have completed the file [PatientsPerWeekHistogram.java](./src/main/java/patient/PatientsPerWeekHistogram.java) which produces the following output: **No**

### Problem 5.2

**a)** I have completed the file [BinaryPatientTree.java](./src/main/java/patient/BinaryPatientTree.java): **Yes** 

Approach: Starting at the root node, the aim was to find the first node (of type KeyPairValue) whose Key started with the String s passed by argument. From here, we could then output that node, and recursively find the next nodes from the left and right children of the matching node.

This was achieved by iterating through the BinaryTree, maintaining a single BinaryTreeNode at any given time, and comparing the input string to this node. By storing the single BinaryTreeNode, the storage costs could be limited.

At each node, the method checks whether the key starts with the argument string s, returning the node as a match if so. Else, we use the compareTo() method, to compare the strings: if less than 0, we traverse the right tree, else we traverse the left tree.

By traversing through a Binary Tree, the depth of our search was limited to O(log(n)) time, also allowing us to disregard a large amount of the tree whilst traversing. 

<div class='marker_feedback'>Marker feedback:</div>

**b)** Results:

Again, I will display the last 20 results from the console output, patients whose names start with 'Fu':

4779270213 	Fulton 	Grace 	28/01/2018 	CACGGAAAAGATGCTGGT 	

4809063231 	Fulton 	Bentley 	31/01/2018 	CACGGAAAAGATGCTGGT 	

5757210516 	Fulton 	Allison 	02/02/2018 	CACGGAAAAGATGCTGGT 	

4656229864 	Fulton 	Oliver 	03/02/2018 	CACGGGAAAGATGCTGGT 	

1260764709 	Fulton 	Xavier 	02/02/2018 	CACGGAAAAGATGCTGGT 	

3452349058 	Fulton 	Hannah 	30/01/2018 	CACGGAAAAGATGCTGGT 	

3648649378 	Fulton 	Benjamin 	01/02/2018 	CACGGAAAAGATGCTGGT 	

3965251829 	Fulton 	Colton 	29/01/2018 	CACGGAAAAGATGCTGGT 	

4571795540 	Fulton 	Carson 	02/02/2018 	CACGGAAAAGATGCTGGT 	

3519915115 	Fulton 	Charlotte 	02/02/2018 	CACGGAAAAGATGCTGGT 	

1984248620 	Fulton 	Kevin 	01/02/2018 	CACGGAAAAGATGCTGGT 	

2746001814 	Fulton 	Zoe 	02/02/2018 	CACGGAAAAGATGCTGGT 	

3143458083 	Fulton 	Elijah 	05/02/2018 	CACGGAAAAGATGCTGGT 	

1813037688 	Fulton 	Nolan 	03/02/2018 	CACGGAAAAGATGCTGGT 	

204060708 	Fulton 	Kayla 	03/02/2018 	CACGGAAAAGATGCTGGT 	

715363908 	Fulton 	Hailey 	05/02/2018 	CACGGAAAAGATGCTGGT 	

1084421231 	Fulton 	Madeline 	02/02/2018 	CACGGAAAAGATGCTGGT 	

1126418194 	Fulton 	Lillian 	01/02/2018 	CACGGAAAAGATGCTGGT 	

299762414 	Fulton 	Lydia 	29/01/2018 	CACGGAAAAGATGCTGGT 	

6164331092 	Furter 	Frank_N 	02/02/2018 	CACGGGAAAGATGTTCGT 	

Approach: In order to make use of the BinaryTree Search, we first needed to convert the given PatientsArrayList into a structure representing a binary tree.

This was achieved by sorting the list, using the Merge Sort function implemented earlier (by lastname), and generating the Binary Tree using the private helper method: createTree(PatientArrayList patients, int first, int last). This would take the mid element in the range, as the current node, and recursively call the createTree() method to find the generate the left and right subtrees of the current node. 

After creating the Binary Tree, we could then apply the search algorithm described in part a).
<div class='marker_feedback'>Marker feedback:</div>
