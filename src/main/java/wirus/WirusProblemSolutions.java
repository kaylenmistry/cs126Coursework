package wirus;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import adt.set.Set;
import adt.map.HashMap;
import patient.BinaryPatientTree;
import patient.Patient;
import patient.PatientArrayList;
import patient.PatientFile;
import patient.IPatientSort;

import adt.map.KeyValuePair;
import adt.binarytree.BinaryTreeNode;


/**
 * Assignment solution.
 *
 * <p>Please complete the methods in this class, by providing solutions to the problems specified in
 * Wirus.pdf. You must
 * not modify the signatures of these methods (name, parameters, modifiers, etc). Note also that:
 *
 * <ol>
 * <li>All methods are defined as static, therefore they should contain independent and standalone
 * code.
 * <li>Take care about returning correct values. We will use an automated test suite to validate
 * your results.
 * <li>Any output directed to the console will be ignored when testing your program, you can freely
 * use it for your
 * own purposes.
 * <li>The application entry, which contains main(), is in {@link Wirus}, which you can use for
 * running your program and calling the methods in {@link WirusProblemSolutions}. Please note
 * that most methods require {@link PatientArrayList} as parameter, which is returned by
 * {@link #problem2()}. The program
 * flow proposed in {@link Wirus} is correct as long as you do not modify the contents of
 * {@link PatientArrayList}
 * in any of these methods (as Lists are passed as references).
 * </ol>
 *
 * <p>PLEASE MODIFY
 *
 */
public class WirusProblemSolutions {

  public static String directory = "data/";
  public static String file = "u1736245.dat";

  /**
   * Please put your solution of problem 2 here.
   *
   * @return Sorted by ID list of patients read from file.
   * @throws ParseException
   * @throws IOException
   */
  public static PatientArrayList problem2() throws IOException, ParseException {

    // Calls PatientFile.read() and stores the results in patients array.
    PatientArrayList patients = PatientFile.read(directory + file);
    System.out.print("\n\n\n\nProblem 2\n\n\n\n");

    // Sort the patients array on ID
    patients.sort(IPatientSort.PatientSortableFields.ID);
    // Outputs the sorted patients array, returns such array
    System.out.println(patients.toString());
    return patients;
  }

  /**
   * Please put your solution of problem 3a here.
   *
   * @param patients read in from file. This could for example be the output from
   *        {@link #problem2()} as shown in {@link Wirus}
   * @return Set virus sequences
   * @throws IOException
   * @throws ParseException
   */
  public static Set<String> problem3a(PatientArrayList patients)
          throws IOException, ParseException {

    Set<String> dnaset = new Set<>();
    System.out.print("\n\n\n\nProblem 3a\n\n\n\n");

    // For each patient, add their virus sequence to the set if unique thus far
    for (Patient patient : patients) {
      dnaset.add(patient.getVirusSequence());
    }
    System.out.println(dnaset.toList().toString());
    // Returns the set of virus sequences
    return dnaset;
  }

  /**
   * Please put your solution of problem 3c here.
   *
   * @param patients read in from file. This could for example be the output from
   *        {@link #problem2()} as shown in {@link Wirus}
   * @return List of patients with specified virus sequence. Please create new
   *         {@link PatientArrayList} and add to it patients that meet criteria.
   * @throws IOException
   * @throws ParseException
   */
  public static PatientArrayList problem3c(PatientArrayList patients)
          throws IOException, ParseException {

    PatientArrayList patientsWithSequence = new PatientArrayList();
    System.out.print("\n\n\n\nProblem 3c\n\n\n\n");

    // Initialise variables, declare specified virus sequence
    String sequence = "CACGGGAAAGATGTTCGT";

    // For each patient in the patient array list, check if their virus sequence matches the specified sequence
    for(Patient patient : patients) {
      if (patient.getVirusSequence().equals(sequence)) {
        // If patient has virus sequence, add to array, output patient to console
        patientsWithSequence.add(patient);
        System.out.println(patient.toString());
      }
    }

    // Problem 3c: find patients with the mysterious sequence using brute force

    return patientsWithSequence;
  }

  /**
   * Please put your solution of problem 4 here.
   *
   * @param patients read in from file. This could for example be the output from
   *        {@link #problem2()} as shown in {@link Wirus}
   * @return Patient with specified ID
   */
  public static Patient problem4(PatientArrayList patients) {

    System.out.print("\n\n\n\nProblem 4\n\n\n\n");
    Patient identifiedPatient;
    Long id = 6164331092L;

    // Hash map storing patients with id as the key
    HashMap<Long, Patient> patientMap = new HashMap<>();

    Long currentKey;
    for (Patient patient : patients) {
      // Adds the <id, patient> pair to the hash map
      currentKey = patient.getId();
      patientMap.add(currentKey, patient);
    }
    // Gets the patient with the matching id, output them to console.
    identifiedPatient = patientMap.get(id);
    System.out.println(identifiedPatient.toString());
    // Return the identified patient
    return identifiedPatient;

  }

  /**
   * Please put your solution of problem 5.1 here.
   *
   * @param patients read in from file. This could for example be the output from
   *        {@link #problem2()} as shown in {@link Wirus}
   * @return Map<Date, num of patients>
   * @throws ParseException
   */
  public static java.util.Map<Date, Integer> problem5_1(PatientArrayList patients)
          throws ParseException {

    java.util.Map<Date, Integer> map = null;

    System.out.print("\n\n\n\nProblem 5 - Option 1\n\n\n\n");

    // Possible strategy: use a HashMap (java.util.HashMap<Date, Integer>) and/or a TreeMap
    // (java.util.Map<Date, Integer>), or other classes of the Java Collections Framework which you
    // think are appropriate.

    // Before adding a date to a Map, create a String from the patient file using the
    // SimpleDateFormat "w/y".
    // Subsequently, parse that String to convert it back to a Date object, which will effectively
    // bin all
    // dates that occur within one week to have the Date of "w/y".

    // The Integer value that is stored in the HashMap/TreeMap together with the Date key
    // should be incremented whenever you add a new Date for which entries already
    // exist.

    // If using a HashMap you can easily generate a sorted TreeMap from it (java.util.Map<Date,
    // Integer> treeMap).

    // All that remains to be done is to iterate over the members of the TreeMap and
    // print them.

    // Note that you must return a java.util.Map<Date, Integer> structure, which is sorted

    // You can use your very own approach to find the solution to this problem.
    // However, you must return a sorted java.util.Map<Date, Integer> structure for automated
    // testing your results.
    return map;

  }

  /**
   * Please put your solution of problem 5.2 here.
   *
   * <p>Please note implementation requirements for {@link BinaryPatientTree} class.
   *
   * @param patients read in from file. This could for example be the output from
   *        {@link #problem2()} as shown in {@link Wirus}
   * @return List of patients whose first letters match a specific string
   * @see BinaryPatientTree
   */
  public static PatientArrayList problem5_2(PatientArrayList patients) {
    System.out.print("\n\n\n\nProblem 5 - Option 2\n\n\n\n");
    // Sorting patients list by lastname
    patients.sort(IPatientSort.PatientSortableFields.LASTNAME);
    // Create the binary tree from the sorted patients list
    BinaryPatientTree pt = new BinaryPatientTree();
    pt.setRoot(createBinaryTree(patients, 0, patients.size()));
    // Example, patients whose names start with 'Fu'
    pt.findPatientsWhoseNamesStartWith("Fu");

    return pt.getFoundRecords();
  }

  // Private helper method for creating the binary tree
  private static BinaryTreeNode<KeyValuePair<String, Patient>> createBinaryTree(PatientArrayList patients, int first, int last) {
    // If the same element is pointed to
    if (first == last) { return null; }
    int mid = (first + last) / 2;
    // Create the binary tree node
    Patient patient = patients.get(mid);
    KeyValuePair<String, Patient> pair = new KeyValuePair(patient.getLastName(), patient);
    BinaryTreeNode<KeyValuePair<String, Patient>> node = new BinaryTreeNode<>(pair);
    // Recursively set the current node's left and right nodes, and return the node
    node.setLeft(createBinaryTree(patients, first, mid));
    node.setRight(createBinaryTree(patients, mid + 1, last));
    return node;
  }

}
