package patient;

import adt.binarytree.BinaryTree;
import adt.binarytree.BinaryTreeNode;
import adt.map.KeyValuePair;

/**
 * Implementation requirement: Each found patient must be added to {@link #foundRecords} list.
 *
 * @author by Till Bretschneider
 */
public class BinaryPatientTree extends BinaryTree<KeyValuePair<String, Patient>> {
  /**
   * Store found patients here.
   */
  private PatientArrayList foundRecords = new PatientArrayList();

  /**
   * Find patients whose names start with specified string.
   *
   * @param s pattern to search for
   * @return true if found at least one name, false otherwise
   */
  public boolean findPatientsWhoseNamesStartWith(String s) {

    // find first node whose first characters match s
    BinaryTreeNode<KeyValuePair<String, Patient>> n = firstNodeStartingWith(getRoot(), s);
    if (n == null) {
      System.out.println("Names starting with \"" + s + "\" not found");
      return false;
    } else {
      outputPatientsWhoseNamesStartWith(n, s);
      return true;
    }
  }

  private BinaryTreeNode<KeyValuePair<String, Patient>> firstNodeStartingWith(
          BinaryTreeNode<KeyValuePair<String, Patient>> root, String s) {

    BinaryTreeNode<KeyValuePair<String, Patient>> current = root;
    while(current != null) {
      String key = current.getValue().getKey();
      if (key.startsWith(s)) {
        foundRecords.add(current.getValue().getValue());
        return current;
      } else if (key.compareTo(s) < 0) {
        current = current.getRight();
      } else {
        current = current.getLeft();
      }
    }
    return null;
  }

  /**
   *
   * <p>Please add each found patient record to {@link #foundRecords}, which is required for
   * automated testing. If you want, you can also print out those records to the console for
   * debugging.
   *
   * @param n
   * @param s
   */
  protected void outputPatientsWhoseNamesStartWith(BinaryTreeNode<KeyValuePair<String, Patient>> n,
          String s) {
    BinaryTreeNode<KeyValuePair<String, Patient>> firstNode = firstNodeStartingWith(n, s);
    if (firstNode != null) {
      // Output the patient
      System.out.println(firstNode.getValue().getValue().toString());
      outputPatientsWhoseNamesStartWith(firstNode.getLeft(), s);
      outputPatientsWhoseNamesStartWith(firstNode.getRight(), s);
    }
  }

  /**
   * Return records of patients found by {@link #findPatientsWhoseNamesStartWith(String)}
   *
   * <p>DO NOT MODIFY.
   *
   * @return the foundRecords
   */
  public PatientArrayList getFoundRecords() {
    return foundRecords;
  }

}
