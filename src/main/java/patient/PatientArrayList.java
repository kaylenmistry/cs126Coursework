package patient;

import java.util.Comparator;

import adt.list.MyArrayList;

/**
 * Implementation of a sortable list of patient's records.
 *
 * <p>This is a specific implementation of {@link MyArrayList} extended by sorting records by
 * specified field.
 *
 * <p>PLEASE COMPLETE
 *
 */
public class PatientArrayList extends MyArrayList<Patient> implements IPatientSort {

  private Comparator<Patient> comp;

  /**
   * Create empty list.
   */
  public PatientArrayList() {
    super();
  }

  /*
   * (non-Javadoc)
   *
   * @see patient.IPatientSort#sort(patient.IPatientSort.PatientSortableFields)
   */
  @Override
  public void sort(PatientSortableFields name) {
    comp = setComp(name);
    if (!super.isEmpty()) {
      mergeSort(0, (super.size() - 1));
    }
  }

  private void mergeSort(int low, int high) {
    if (low < high) {
      int mid = (low + high) / 2;
      mergeSort(low, mid);
      mergeSort(mid + 1, high);

      int endLow = mid;
      int startHigh = mid + 1;
      while ((low <= endLow) && (startHigh <= high)) {
        if (comp.compare(super.get(low), super.get(startHigh)) < 0) {
          low++;
        } else {
          Patient temp = super.get(startHigh);
          for (int i=startHigh; i>low; i--) {
            super.set(i, super.get(i - 1));
          }
          super.set(low, temp);
          low++;
          endLow++;
          startHigh++;
        }
      }
    }
  }

  /**
   * Compare patients records by date of registration.
   *
   */
  public class DateComparator implements Comparator<Patient> {
    @Override
    public int compare(Patient a, Patient b) throws ClassCastException {
      return (a.getRegistrationDate().compareTo(b.getRegistrationDate()));
    }
  }

  /**
   * Compare patients records by ID.
   *
   */
  public class IdComparator implements Comparator<Patient> {
    @Override
    public int compare(Patient a, Patient b) throws ClassCastException {
      return (a.getId().compareTo(b.getId()));
    }
  }

  /**
   * Compare patients records by name.
   *
   */
  public class NameComparator implements Comparator<Patient> {
    @Override
    public int compare(Patient a, Patient b) throws ClassCastException {
      return (a.getLastName().compareTo(b.getLastName()));
    }
  }

  /**
   * Create and return specified comparator.
   *
   * @param name name of requested comparator
   * @return instance of the comparator
   */
  public Comparator<Patient> setComp(PatientSortableFields name) {
    switch(name) {
      case LASTNAME:
        return new NameComparator();
      case ID:
        return new IdComparator();
      default:
        return new DateComparator();
    }
  }

  /*
   * (non-Javadoc)
   * PLEASE DO NOT MODIFY
   *
   * @see adt.list.MyArrayList#toString()
   */
  @Override
  public String toString() {
    if (this.isEmpty())
      return "[]";
    StringBuilder ret = new StringBuilder("ID \tLastname \tFirstname \tDate \tSequence\n");
    for (Patient p : this)
      ret.append(p.toString());
    return ret.toString();
  }

}
