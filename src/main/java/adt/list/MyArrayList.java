package adt.list;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * A generic implementation of the IList interface.
 *
 * <p>MODIFY THIS FILE. Based on MyArrayList.java from CS126 lab 1. See adt/Readme.md
 *
 * @author Till Bretschneider
 *
 * @param <E> the type of elements in this list.
 */
public class MyArrayList<E> implements IList<E>, Iterable<E> {

  private Object[] array;
  private int size;
  private int capacity;

  /**
   * Create empty list with initial capacity.
   */
  public MyArrayList() {
    this.capacity = 100;
    this.array = new Object[capacity];
    this.size = 0;
  }

  /*
   * (non-Javadoc)
   *
   * @see adt.list.IList#add(java.lang.Object)
   */
  @Override
  public boolean add(E element) {
    // Adds element to the list, returns true on success, resizes otherwise
    if (size == capacity) {
      capacity *= 2;
      Object[] newArray = new Object[capacity];
      System.arraycopy(array, 0, newArray, 0, array.length);
      array = newArray;
    }
    array[size] = element;
    size++;
    return true;
  }

  /*
   * (non-Javadoc)
   *
   * @see adt.list.IList#contains(java.lang.Object)
   */
  @Override
  public boolean contains(E element) {
    // Returns true when element is in the list, false otherwise.
    for (Object item : array) {
      if (element.equals(item)) {
        return true;
      }
    }
    return false;
  }

  /*
   * (non-Javadoc)
   *
   * @see adt.list.IList#clear()
   */
  @Override
  public void clear() {
    this.capacity = 100;
    this.array = new Object[capacity];
    this.size = 0;
  }

  /*
   * (non-Javadoc)
   *
   * @see adt.list.IList#isEmpty()
   */
  @Override
  public boolean isEmpty() {
    return this.size() == 0;
  }

  /*
   * (non-Javadoc)
   *
   * @see adt.list.IList#size()
   */
  @Override
  public int size() {
    return size;
  }

  // This line allows us to cast our object to type (E) without any warnings.
  // For further details, please see:
  // http://docs.oracle.com/javase/1.5.0/docs/api/java/lang/SuppressWarnings.html
  @Override
  @SuppressWarnings("unchecked")
  public E get(int index) {
    return (E) this.array[index];
  }

  /*
   * (non-Javadoc)
   *
   * @see adt.list.IList#indexOf(java.lang.Object)
   */
  @Override
  public int indexOf(E element) {
    for (int i = 0; i < this.size(); i++) {
      if (element.equals(this.get(i))) {
        return i;
      }
    }
    return -1;
  }

  /*
   * (non-Javadoc)
   *
   * @see adt.list.IList#remove(java.lang.Object)
   */
  @Override
  public boolean remove(E element) {
    int index = this.indexOf(element);
    if (index >= 0) {
      E removed = this.get(index);
      for (int i = index + 1; i < this.size(); i++) {
        this.set(i - 1, this.get(i));
      }
      this.array[size - 1] = null;
      size--;
      return true;
    }
    return false;
  }

  /*
   * (non-Javadoc)
   *
   * @see adt.list.IList#set(int, java.lang.Object)
   */
  @Override
  public E set(int index, E element) {
    if (index >= this.size()) {
      throw new ArrayIndexOutOfBoundsException("index > size: " + index + " >= " + size);
    }
    E replaced = this.get(index);
    this.array[index] = element;
    return replaced;
  }

  /*
   * (non-Javadoc)
   * Example for use of for-each loop.
   * modified by Till Bretschneider
   *
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    if (this.isEmpty())
      return "[]";

    String s = "[";

    for (E e : this)
      s += e.toString() + ", ";

    return s.substring(0, s.length() - 2) + "]";
  }

  /*
   * (non-Javadoc)
   *
   * @see java.lang.Iterable#iterator()
   */
  @Override
  public Iterator<E> iterator() {
    return new MyArrayListIterator();
  }

  private class MyArrayListIterator implements Iterator<E> {
    private int next = 0;
    private boolean isRemovable = false;

    @Override
    public boolean hasNext() {
      return (next < size);
    }

    @Override
    public E next() throws NoSuchElementException {
      if (next == size) {
        throw new NoSuchElementException("No next element");
      }
      isRemovable = true;
      return MyArrayList.this.get(next++);
    }

    @Override
    public void remove() throws IllegalStateException {
      if (!isRemovable) {
        throw new IllegalStateException("Nothing to remove");
      }
      MyArrayList.this.set(next - 1, null);
      next--;
      isRemovable = false;
    }

  }

}
